package main

import (
	"net/http"

	"cleanblog/controllers"

	"github.com/gorilla/mux"
)

func main() {

	staticC := controllers.NewStatic()

	r := mux.NewRouter()
	r.Handle("/", staticC.HomeView).Methods("GET")
	http.ListenAndServe(":3000", r)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
