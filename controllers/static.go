package controllers

import "cleanblog/views"

func NewStatic() *Static {
	return &Static{
		HomeView: views.NewView("index", "static/home"),
	}
}

type Static struct {
	HomeView *views.View
}
